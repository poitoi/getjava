public class Test {
    public static void main(String[] args) {
        Candidate candidates[] = new Candidate[4]; // Декларируем массив типа Candidate
        Employer employer = new Employer(); // Создаем объект типа Employer

        candidates[0] = new Candidate1("Alex", 30);
        candidates[1] = new Candidate1("Jane", 24);
        candidates[2] = new Candidate2("Bob", 20);
        candidates[3] = new Candidate2("Mary", 34);

        for (Candidate candidate : candidates) { // Создаем цикл for each для обработки элементов массива
            employer.hello(); // Вызываем методы hello(), showAge(), describeExperience() у интервьюера и кандидатов
            candidate.hello();
            candidate.showAge();
            candidate.describeExperience();
        }
    }
}

interface Talk {  // Создаем интерфейс для классов Employer и Candidate 1,2
    public void hello();
}

abstract class Candidate implements Talk { // Абстрактный класс Candidate реализует интерфейс Talk
    String name;
    int age;

    Candidate(String n, int b) {
        name = n;
        age = b;
    }

    public abstract void describeExperience(); // Декларируем метод, описывающий опыт кандидата
    public abstract void showAge(); // Декларируем метод, который будет показывать сколько лет кандидату
}

class Employer implements Talk { // класс Employer реализует интерфейс Talk
    @Override // Переопределяем метод hello() из интерфейса Talk для Employer
    public void hello() {
        System.out.println("Hi! Introduce yourself and describe your java experience, please");
    }
}

class Candidate1 extends Candidate { // Класс Candidate1 наследует класс Candidate
    Candidate1(String n, int b){
        super(n,b);
    }

    @Override // Переопределяем метод hello() из интерфейса Talk для Candidate1
    public void hello() {
        System.out.println("Hi, my name is " + name);
    }

    @Override // Переопределяем метод showAge() из абстрактного класса Candidate для Candidate1
    public void showAge() {
        System.out.println("I'm " + age + " years old");
    }

    @Override // Переопределяем метод describeExperience() из абстрактного класса Candidate для Candidate1
    public void describeExperience() {
        System.out.println("I passed successfully getJavaJob exams and code reviews");
        System.out.println();
    }
}

class Candidate2 extends Candidate { // Класс Candidate2 наследует класс Candidate
    Candidate2(String n, int b){
        super(n,b);
    }

    @Override // Переопределяем метод hello() из интерфейса Talk для Candidate2
    public void hello() {
        System.out.println("Hi, my name is " + name);
    }

    @Override // Переопределяем метод showAge() из абстрактного класса Candidate для Candidate2
    public void showAge() {
        System.out.println("I'm " + age + " years old");
    }

    @Override // Переопределяем метод describeExperience() из абстрактного класса Candidate для Candidate2
    public void describeExperience() {
        System.out.println("I have been learning Java by myself, nobody examined how thorough is my knowledge and how good is my code");
        System.out.println();
    }
}
